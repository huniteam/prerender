This is a fork of https://github.com/icilalune/prerender

The prerender server itself has been hacked to internally rewrite urls from
scrapers:

  * `#` fragments are removed (some scrapers add these)
  * `/share/$path` is rewritten to `/#/$path`

The application being prerendered uses `/#/` angular paths. Links that are
shared via AddThis get the hash remapped to `/share/`.

This hack works around the whole fragment business, and by lucky coincidence,
makes the whole nginx config much simpler.
